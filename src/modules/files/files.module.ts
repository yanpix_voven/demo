import {Module} from "@nestjs/common";
import {CoreModule} from "../../core/core.module";
import {FileController} from "./file.controller";
import {FileService} from "./files.service";
import {SettingsClass} from "./lib";

@Module({
    imports:[CoreModule],
    providers:[FileService, SettingsClass],
    controllers:[FileController],
    exports:[]
})
export class FilesModule {}
