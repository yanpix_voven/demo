import {FileFieldsInterceptor} from "@nestjs/platform-express";
import { diskStorage } from 'multer';

export const csvInterceptor = FileFieldsInterceptor([
    { name: 'csv', maxCount: 1 },
], {
    storage: diskStorage({
        destination: (req, file, cb) => {
            const dir = process.env.UPLOADS_DIR;
            return cb(null, dir);
        },
        filename: function (req, file, cb) {
            return cb(null, `${file.originalname}`)
        },
    })
});
