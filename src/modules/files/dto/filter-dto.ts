import {IsIn, IsNotEmpty, IsString} from "class-validator";

export class FilterDto {

    @IsNotEmpty()
    @IsString()
    substring: string;

    @IsNotEmpty()
    @IsString()
    @IsIn(['email','phone'], {message: 'not supported column'})
    column: string
}
