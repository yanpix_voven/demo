import {Controller, Post, UploadedFiles, UseInterceptors, Res, ValidationPipe, Body } from "@nestjs/common";
import {FileService} from "./files.service";
import {FilterDto} from "./dto";
import {csvInterceptor} from "./interceptors";
const path = require('path');

@Controller('files')
export class FileController {
    constructor(
        private readonly fileService: FileService,
    ) {}

    @Post('upload')
    @UseInterceptors(csvInterceptor)
    async upload(
        @Res() res,
        @UploadedFiles() files,
        @Body(ValidationPipe) dto: FilterDto
    ){
        const {substring, column} = dto;
        await this.fileService.filter(files.csv[0].path, substring, column);
        return res.download(path.join(process.env.UPLOADS_DIR, process.env.DOWNLOAD_FILENAME), process.env.DOWNLOAD_FILENAME);
    }
}
