export class CsvRowClass {
    constructor(keys:string[], data:string[]) {
        for (const [index, value] of keys.entries()){
            this[value] = data[index];
        }
    }
}

