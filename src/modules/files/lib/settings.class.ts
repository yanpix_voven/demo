import {Injectable, InternalServerErrorException} from "@nestjs/common";

@Injectable()
export class SettingsClass {

    getPath(): string
    {
        return process.env.UPLOADS_DIR || './files';
    }

    getFileName(): string
    {
        return process.env.DOWNLOAD_FILENAME || 'filtered.csv';
    }

    getKeys(): CsvColumn[]
    {
        return ['first_name','last_name','company_name','address','city','county','state','zip','phone1','phone','email'];
    }

    getColumnIndex(column: CsvColumn): number
    {
        const columnIndex = this.getKeys().findIndex(item=>item===column);
        if(columnIndex===-1){
            throw new InternalServerErrorException(`Column not exist (${column})`);
        }
        return columnIndex;
    }
}

declare type CsvColumn = string;
