import {Injectable } from "@nestjs/common";
import {Parser} from 'csv-parse';
import {CsvRowClass, SettingsClass} from "./lib";
const path = require('path');
const fs = require('fs');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;

@Injectable()
export class FileService {

    constructor(private readonly settings: SettingsClass) {}

    /***
     * Read/Write process
     * @param filePath
     * @param substring
     * @param column
     */
    async filter(filePath: string, substring: string, column: string): Promise<void> {
        const columnIndex = this.settings.getColumnIndex(column);
        const records = await this.readFile(filePath, substring, columnIndex);
        await this.writeFile(records);
    }

    /***
     * Filter data
     * @param filePath
     * @param substring
     * @param columnIndex
     */
    async readFile(filePath: string, substring: string, columnIndex: number): Promise<Record<any, any>> {
        const csvData = [];
        const uploadFile = (file) => new Promise((resolve, reject) => {
            fs
                .createReadStream(filePath)
                .pipe(new Parser({delimiter: ',', from_line: 2}))
                .on('data', (csvrow) => {
                    if (csvrow[columnIndex].includes(substring)) {
                        csvData.push(new CsvRowClass(this.settings.getKeys(), csvrow));
                    }
                })
                .on('end', async () => {
                    resolve(csvData);
                })
                .on('error', (err) => {
                    reject(err);
                })
            ;
        });
        return await uploadFile(filePath);
    }

    /***
     * Write filtered data into file
     * @param data
     */
    async writeFile(data: Record<any, any>): Promise<void> {
        const csvWriter = createCsvWriter({
            path: path.join(
                this.settings.getPath(),
                this.settings.getFileName()
            ),
            header: this.settings
                .getKeys()
                .map(item => {
                    return {id: item, title: item}
                })
        });
        await csvWriter.writeRecords(data);
    }
}
