import {Body, Controller, Get, Param, ParseIntPipe, Patch, Post, UseGuards, ValidationPipe} from "@nestjs/common";
import {ProductsProvider} from "../services";
import {AuthGuard} from "@nestjs/passport";
import {ProductCreateDto, ProductEditDto} from "../dto";

@Controller('/products')
export class ProductsController {

    constructor(
        private readonly productsService: ProductsProvider
    ) {}

    @Get('/:id')
    async getProduct(@Param('id', new ParseIntPipe()) id): Promise<Record<any, any>>
    {
        return await this.productsService.get(id);
    }


    @Post('/add')
    @UseGuards(AuthGuard('jwt'))
    async addProduct(@Body(ValidationPipe) dto: ProductCreateDto): Promise<Record<any, any>> {
        return await this.productsService.add(dto);
    }

    @Patch('/:id/edit')
    @UseGuards(AuthGuard('jwt'))
    async editProduct(@Param('id', new ParseIntPipe()) id, @Body(ValidationPipe) dto: ProductEditDto): Promise<Record<any, any>>
    {

        return await this.productsService.edit(id, dto);
    }

}
