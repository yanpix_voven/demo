import {Injectable, InternalServerErrorException} from "@nestjs/common";
import {ProductsService} from "../../../core/services/products.service";
import {ProductCreateDto, ProductEditDto} from "../dto";

@Injectable()
export class ProductsProvider {
    constructor(private readonly productsService: ProductsService) {}

    async get(id: number)
    {
        return await this.productsService.getById({id});
    }

    async add(dto: ProductCreateDto)
    {
        return await this.productsService.addProduct(dto);
    }

    async edit(id: number, dto: ProductEditDto)
    {
        return await this.productsService.editProduct({id}, dto);
    }
}
