import {Module} from "@nestjs/common";
import {PRODUCTS_CONTROLLERS} from "./controllers";
import {ProductsProvider} from "./services";
import {CoreModule} from "../../core/core.module";

@Module({
    imports:[CoreModule],
    providers:[ProductsProvider],
    controllers:[...PRODUCTS_CONTROLLERS],
    exports:[]
})
export class ProductsModule {}
