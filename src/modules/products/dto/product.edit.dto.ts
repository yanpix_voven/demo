import {IsInt, IsNotEmpty, IsOptional, IsPositive} from "class-validator";
import {Type} from "class-transformer";
import {IProduct, IProductEdit} from "../../../core/interfaces";

export class ProductEditDto implements IProductEdit{

    @IsOptional()
    @IsNotEmpty()
    title: string;

    @IsOptional()
    @IsNotEmpty()
    description: string;

    @IsOptional()
    @IsNotEmpty()
    @IsInt()
    @Type(()=>Number)
    @IsPositive()
    price: number;

}
