import {IsInt, IsNotEmpty} from "class-validator";
import {Type} from "class-transformer";
import {IProductCreate} from "../../../core/interfaces";

export class ProductCreateDto implements IProductCreate{

    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    description: string;

    @IsNotEmpty()
    @IsInt()
    @Type(()=>Number)
    price: number;
}
