import {JwtService} from "@nestjs/jwt";
import {Injectable, UnauthorizedException} from "@nestjs/common";
import {IJwtPayload, IUser, IUserService} from "../interfaces";
import {SigninDto, SignupDto, UserPasswordDto} from "../dto";
import {UserService} from "../../../core/services/user.service";

@Injectable()
export class AuthService
{
    private readonly userService: IUserService;

    constructor(
        private readonly jwtService: JwtService,
        userService: UserService
    ) {
        this.userService = userService;
    }

    async getBy(dto: Partial<SignupDto>, ...args:any[]): Promise<IUser>
    {
        return await this.userService.getBy(dto);
    }

    async create(dto: SignupDto): Promise<IUser>
    {
        return await this.userService.create(dto);
    }

    async updatePassword(user: IUser, dto: UserPasswordDto): Promise<boolean>
    {

        return await this.userService.updatePassword({username: user.username, password: dto.password});
    }

    async getJwt(dto: SigninDto): Promise<Jwt>
    {
        const username = await this.userService.validate(dto);
        if(!username){
            throw new UnauthorizedException('Invalid creds');
        }
        const payload: IJwtPayload = {username: dto.username};
        const jwt = this.jwtService.sign(payload);
        return {jwt};
    }

}

export declare type Jwt = {
    jwt: string
}
