import {PassportModule} from '@nestjs/passport';
import {JwtModule} from '@nestjs/jwt';
import {Module} from "@nestjs/common";
import {AuthService} from "./services";
import {JwtStrategy} from "./strategies/jwt.strategy";
import {AUTH_CONTROLLERS} from "./controllers";
import {CoreModule} from "../../core/core.module";
import {ConfigModule} from "@nestjs/config";

@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        PassportModule.register({defaultStrategy: 'jwt'}),
        JwtModule.register({
            secret: process.env.JWT_SECRET ,
            signOptions: {
                expiresIn: process.env.JWT_EXPIRES
            }
        }),
        CoreModule,
    ],
    controllers: [...AUTH_CONTROLLERS],
    providers: [
        JwtStrategy,
        AuthService,
    ],
    exports: [
        JwtStrategy,
        PassportModule,
    ],
})
export class AuthModule {
}
