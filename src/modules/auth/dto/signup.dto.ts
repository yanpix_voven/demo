import {IsNotEmpty, IsString} from "class-validator";
import {UserCredsDto} from "./user-creds.dto";

export class SignupDto extends UserCredsDto
{
    @IsNotEmpty()
    @IsString()
    lastname: string;

    @IsNotEmpty()
    @IsString()
    firstname: string;
}
