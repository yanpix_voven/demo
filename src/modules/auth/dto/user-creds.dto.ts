import {UserPasswordDto} from "./user-password.dto";
import {IsNotEmpty, IsString} from "class-validator";

export class UserCredsDto extends UserPasswordDto
{
    @IsNotEmpty()
    @IsString()
    username: string;
}
