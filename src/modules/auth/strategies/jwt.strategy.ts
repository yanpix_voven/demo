import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import {Strategy, ExtractJwt} from 'passport-jwt';
import {IJwtPayload, IUser} from "../interfaces";
import {AuthService} from "../services";


@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy)
{
    constructor(private readonly authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env.JWT_SECRET || 'secretJWT'
        });
    }

    async validate(payload: IJwtPayload): Promise<IUser>
    {
        const {username} = payload;
        const user = await this.authService.getBy({username});
        if(!user){
            throw new UnauthorizedException('jwt validate false');
        }
        return user;
    }
}
