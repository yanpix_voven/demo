import {Body, Controller, Post, Put, UseGuards, UseInterceptors, ValidationPipe} from "@nestjs/common";
import {AuthGuard} from "@nestjs/passport";
import {SigninDto, SignupDto, UserPasswordDto} from "../dto";
import {AuthService, Jwt} from "../services";
import {IUser} from "../interfaces";
import {TransformInterceptor} from "../interceptors/transform.interceptor";
import {GetUser} from "../../../core/decorators";

@Controller('auth')
export class AuthController {

    constructor(
        private readonly authService: AuthService,
    ) {}

    @Post('/signup')
    @UseInterceptors(TransformInterceptor)
    async signUp(@Body(ValidationPipe) dto: SignupDto): Promise<UserCreated>
    {
        return await this.authService.create(dto);
    }

    @Post('/signin')
    async signIn(@Body(ValidationPipe) dto: SigninDto): Promise<Jwt>
    {
        return await this.authService.getJwt(dto);
    }

    @Put('/change-password')
    @UseGuards(AuthGuard('jwt'))
    async changePwd(
        @GetUser() user: IUser,
        @Body(ValidationPipe) dto: UserPasswordDto): Promise<any>
    {
        return await this.authService.updatePassword(user, dto)
    }
}

declare type UserCreated = Partial<IUser> & {
    id: number;
}


