import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Response<T> {
    data: T;
}

@Injectable()
export class TransformInterceptor<T> implements NestInterceptor<T, Response<T>> {
    intercept(context: ExecutionContext, next: CallHandler): Observable<Response<T>> {
        return next
            .handle()
            .pipe(
                map(value => {
                    if(value.hasOwnProperty('password')) {
                        delete value.password;
                    }
                    if(value.hasOwnProperty('salt')) {
                        delete value.salt;
                    }
                    return value;
               })
            );
    }
}
