import {IUser} from "./user.interface";

export interface IUserService {
    getBy(dto: any): Promise<IUser>
    create(dto: any): Promise<IUser>
    updatePassword(dto: any): Promise<boolean>
    validate(dto: any): Promise<boolean>
}
