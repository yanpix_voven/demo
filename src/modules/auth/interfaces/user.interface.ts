export interface IUser {
    id: number;
    username:string;
    lastname: string;
    firstname: string;
    password: string;
    reauth?:number;
}
