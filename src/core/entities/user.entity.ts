import {BaseEntity, Column, Entity, PrimaryGeneratedColumn, Unique} from 'typeorm';

@Entity({name: 'users'})
@Unique(['username'])
export class UserEntity extends BaseEntity
{
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  username: string;

  @Column()
  password: string;

  @Column()
  salt: string;

  @Column({nullable:true})
  firstname: string;

  @Column({nullable:true})
  lastname: string;

}

