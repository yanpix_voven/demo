import {getManager} from "typeorm";
import {UserEntity} from "../entities";
import {BadRequestException} from "@nestjs/common";

export function CreateUserValidate() {
    return (target: any, propertyKey: string, propertyDescriptor: PropertyDescriptor) => {
        const originalMethod = propertyDescriptor.value;
        propertyDescriptor.value = async function(...args: any[]) {
            const username = args[0].username;
            const user = await getManager().getRepository(UserEntity).findOne({username});
            if(user){
                throw new BadRequestException(`User (${username}) exist`);
            }
            return originalMethod.apply(this, args);
        }
    }
}
