export interface IUserCreate extends IUserCreds {
    lastname: string;
    firstname: string;
}
export interface IUserCreds extends IUserNickname, IUserPassword{}

export interface IUserPassword  {
    password: string;
}

export interface IUserNickname {
    username: string;
}
