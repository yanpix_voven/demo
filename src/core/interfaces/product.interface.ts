export interface IProduct {
    price: number;
    title: string;
    description: string;
}

export interface IProductId {
    id: number;
}



export interface IProductCreate extends IProduct{

}

export interface IProductEdit extends IProduct{

}
