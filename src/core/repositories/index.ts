import {UserRepository} from "./user.repository";
import {ProductRepository} from "./product.repository";

export const CORE_REPOSITORIES = [
    UserRepository,
    ProductRepository,
];
