import {BadRequestException, Injectable, InternalServerErrorException, NotFoundException} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {UserRepository} from "../repositories/user.repository";
import {UserEntity} from "../entities";
import {CreateUserValidate} from "../decorators";
import {UserPasswordService} from "./user-password.service";
import {IUserCreate, IUserCreds}  from "../interfaces";

@Injectable()
export class UserService {

    constructor(
        private readonly passwordService: UserPasswordService,
        @InjectRepository(UserRepository) private readonly userRepository: UserRepository
    ) {}

    @CreateUserValidate()
    async create(dto: IUserCreate): Promise<UserEntity>
    {
        const {username, lastname, firstname, password} = dto;
        try {
            const salt = await this.passwordService.genSalt();
            const entity = this.userRepository.create({username, lastname, firstname, salt});
            entity.password = await this.passwordService.hash(password, salt);
            return  await this.userRepository.save(entity);
        } catch (e) {
            throw new BadRequestException(`Can't create user` + e.message)
        }
    }

    async getBy(dto: Partial<IUserCreate>): Promise<UserEntity>
    {
        const {username} = dto;
        try {
            return await this.userRepository.findOneOrFail({username});
        } catch (e) {
            throw new NotFoundException(`User not found`);
        }
    }

    async updatePassword(dto: IUserCreds): Promise<boolean>
    {
        const {username, password} = dto;
        const user = await this.getBy({username});
        user.password = await this.passwordService.hash(password, user.salt);
        try {
            await this.userRepository.save(user);
            return true;
        } catch (e) {
            throw new InternalServerErrorException(`Can't update password`);
        }

    }

    async validate(dto: IUserCreds): Promise<boolean>
    {
        const user = await this.getBy({username: dto.username});
        return await this.passwordService.validate(dto.password, user.salt, user.password);
    }
}



