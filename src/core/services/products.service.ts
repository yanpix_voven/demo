import {Injectable, InternalServerErrorException} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {ProductRepository} from "../repositories/product.repository";
import {IProduct, IProductCreate, IProductEdit, IProductId} from "../interfaces";
import {UpdateResult} from "typeorm/query-builder/result/UpdateResult";

@Injectable()
export class ProductsService {
    constructor(
        @InjectRepository(ProductRepository) private readonly productsRepository: ProductRepository
    ) {}

    /***
     *
     * @param dto
     */
    async getById(dto: IProductId): Promise<IProduct>
    {
        const {id} = dto;
        try {
            return await this.productsRepository.findOneOrFail(id);
        } catch (e) {
            throw new InternalServerErrorException(`Product (${id}) not found`)
        }

    }

    /***
     *
     * @param dto
     * @param args
     */
    async addProduct(dto: IProductCreate, ...args:any[]): Promise<IProduct>
    {
        const entity = this.productsRepository.create(dto);
        try {
            return await entity.save();
        } catch (e) {
            throw new InternalServerErrorException(`Can't add product.`);
        }
    }

    /***
     *
     * @param id
     * @param dto
     * @param args
     */
    async editProduct(id: IProductId, dto: IProductEdit, ...args:any[]): Promise<UpdateResult>
    {
        const entity = await this.getById(id);
        try {
            return await this.productsRepository.update(entity, dto);
        } catch (e) {
            throw new InternalServerErrorException(`Can't add product.`);
        }
    }

}
