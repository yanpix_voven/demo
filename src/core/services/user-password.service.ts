import {Injectable} from "@nestjs/common";
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserPasswordService {

    async genSalt(): Promise<string>
    {
        return await bcrypt.genSalt();
    }

    async hash(word: string, salt?:string): Promise<string>
    {
        if(!salt){
            salt = await this.genSalt();
        }
        return await bcrypt.hash(word, salt);
    }

    async validate(input:string, salt: string, password: string): Promise<boolean>
    {
        const hash = await bcrypt.hash(input, salt);
        return hash === password;
    }
}
