import { TypeOrmModuleOptions } from '@nestjs/typeorm';

export const typeOrmConfigMysql: TypeOrmModuleOptions = {

  type: 'mysql',
  host: process.env.DB_HOST,
  port:  parseInt(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PWD,
  database: process.env.DB_NAME,
  entities: ['src/../**/*.entity.js'],
  synchronize: true,
  logging: false,
  migrations: ['src/migration/*.js'],
  migrationsTableName: 'migration',
  cli: {
    migrationsDir: 'src/migration',
  },
};

