import {TypeOrmModule} from "@nestjs/typeorm";
import {Global, Module} from "@nestjs/common";
import {UserService} from "./services/user.service";
import {CORE_REPOSITORIES} from "./repositories";
import {UserPasswordService} from "./services/user-password.service";
import {ProductsService} from "./services/products.service";

@Global()
@Module({
    imports: [
        TypeOrmModule.forFeature([...CORE_REPOSITORIES]),
    ],
    providers: [UserService, UserPasswordService, ProductsService],
    exports: [
        UserService,
        UserPasswordService,
        ProductsService
    ],
})
export class CoreModule {}
