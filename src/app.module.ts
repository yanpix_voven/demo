import { Module } from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {ConfigModule} from "@nestjs/config";
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {CoreModule} from "./core/core.module";
import {AuthModule} from "./modules/auth/auth.module";
import {typeOrmConfigMysql} from "./core/config/typeorm.config";
import {ProductsModule} from "./modules/products/products.module";
import {FilesModule} from "./modules/files/files.module";

@Module({
  imports: [
      ConfigModule.forRoot({isGlobal: true}),
      TypeOrmModule.forRoot(typeOrmConfigMysql),
      CoreModule,
      AuthModule,
      ProductsModule,
      FilesModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
